﻿using System;
using System.IO;
namespace AoC2015
{
    public class Day3 : IDay
    {
        private string input;
        public void Setup()
        {
            input = File.ReadAllText("Day3/input.txt");
        }

        public void RunFirst()
        {
            int[,] houses = new int[10000, 10000];
            int x, y;
            x = y = 5000;
            houses[x, y] = 1;
            int housesVisited = 1;
            foreach (char symbol in input)
            {
                if (symbol.Equals('<'))
                    x--;
                if (symbol.Equals('^'))
                    y++;
                if (symbol.Equals('>'))
                    x++;
                if (symbol.Equals('v'))
                    y--;
                if (houses[x, y] == 0)
                {
                    housesVisited++;
                }
                houses[x, y]++;
            }
            Console.WriteLine(housesVisited);
        }

        public void RunSecond()
        {
            int[,] houses = new int[10000, 10000];
            int santaX, santaY, roboX, roboY;
            santaX = santaY = roboX = roboY = 4999;
            houses[4999, 4999] = 1;
            int housesVisited = 1;
            bool santasTurn = true;
            foreach (char symbol in input)
            {
                if (symbol.Equals('<'))
                    if (santasTurn)
                        santaX--;
                    else roboX--;
                if (symbol.Equals('^'))
                    if (santasTurn)
                        santaY++;
                    else roboY++;
                if (symbol.Equals('>'))
                    if (santasTurn)
                        santaX++;
                    else roboX++;
                if (symbol.Equals('v'))
                    if (santasTurn)
                        santaY--;
                    else roboY--;

                if (santasTurn)
                {
                    if (houses[santaX, santaY] == 0)
                        housesVisited++;
                    houses[santaX, santaY]++;   
                }
                else
                {
                    if (houses[roboX, roboY] == 0)
                        housesVisited++;
                    houses[roboX, roboY]++;
                }
                santasTurn = !santasTurn;
            }
            Console.WriteLine(housesVisited);
        }
    }
}