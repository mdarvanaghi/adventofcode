﻿using System;
using System.IO;
namespace AoC2015
{
    public class Day1 : IDay
    {
        private string input;
        public void Setup()
        {
            input = File.ReadAllText("Day1/input.txt");
        }

        public void RunFirst()
        {
            int counter = 0;
            foreach (char symbol in input)
            {
                if (symbol.Equals('(')) counter++;
                else counter--;
            }
            Console.WriteLine(counter);
        }

        public void RunSecond()
        {
            int counter = 1;
            int floor = 0;
            foreach (char symbol in input)
            {
                if (symbol.Equals('(')) floor++;
                else floor--;
                if (floor < 0)
                {
                    Console.WriteLine(counter);
                    return;
                }
                counter++;
            }
        }
    }
}