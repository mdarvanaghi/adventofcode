﻿using System;
using System.IO;
namespace AoC2015
{
    public class Day2 : IDay
    {
        private string[] input;
        public void Setup()
        {
            input = File.ReadAllLines("Day2/input.txt");
        }

        public void RunFirst()
        {
            int totalSquareFeet = 0;
            foreach (string line in input)
            {
                int l, w, h;
                string[] dimensions = line.Split('x');
                l = int.Parse(dimensions[0]);
                w = int.Parse(dimensions[1]);
                h = int.Parse(dimensions[2]);
                int side1 = 2 * l * w;
                int side2 = 2 * l * h;
                int side3 = 2 * w * h;
                int smallest = Math.Min(side1, Math.Min(side2, side3)) / 2;
                int squareFeet = side1 + side2 + side3 + smallest;
                totalSquareFeet += squareFeet;
            }
            Console.WriteLine(totalSquareFeet);
        }

        public void RunSecond()
        {
            int totalFeetOfRibbon = 0;
            foreach (string line in input)
            {
                int l, w, h;
                string[] dimensions = line.Split('x');
                l = int.Parse(dimensions[0]);
                w = int.Parse(dimensions[1]);
                h = int.Parse(dimensions[2]);
                int roundLength = 2 * l + 2 * w + 2 * h;
                roundLength -= Math.Max(l, Math.Max(w, h)) * 2;
                int cubicFeet = l * w * h;
                totalFeetOfRibbon += roundLength + cubicFeet;
            }
            Console.WriteLine(totalFeetOfRibbon);
        }
    }
}