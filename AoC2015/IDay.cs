﻿namespace AoC2015
{
    public interface IDay
    {
        void Setup();
        void RunFirst();
        void RunSecond();
    }
}