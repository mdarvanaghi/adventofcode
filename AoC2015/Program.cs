﻿using System;

namespace AoC2015
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Please enter day and number.");
                return;
            }
            Type dayType = Type.GetType("AoC2015.Day" + args[0]);
            IDay day = (IDay) Activator.CreateInstance(dayType);
            day.Setup();
            int num = int.Parse(args[1]);
            if (num == 1)
            {
                day.RunFirst();
                return;
            }
            day.RunSecond();
        }
    }
}