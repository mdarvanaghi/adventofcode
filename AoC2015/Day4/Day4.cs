﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace AoC2015
{
    public class Day4 : IDay
    {
        private static int numsPerThread = 500000;
        private static string secretKey = "yzbqklnj";
        private static MD5 md5Hash;
        
        public void Setup()
        {
            md5Hash = MD5.Create();
        }

        public void RunFirst()
        {
            for (int i = 0;  i < 32; i++)
            {
                Thread thread = new Thread(() => Mine(i * numsPerThread));
                thread.Start();
            }
        }

        public void RunSecond()
        {
            throw new System.NotImplementedException();
        }
        
        private static string GetMd5Hash(string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private static bool Mine(int start)
        {
            Console.WriteLine("Thread starting at " + start);
            for (int answer = start; answer < start + numsPerThread; answer++)
            {
                string input = secretKey + answer;
                string hash = GetMd5Hash(input);
                bool legit = true;
                for (int i = 0; i < 5; i++)
                {
                    if (hash[i] != 0)
                    {
                        legit = false;
                        break;
                    }
                }

                if (legit)
                {
                    Console.WriteLine(answer);
                    break;
                }
                answer++;
            }
            return false;
        }
    }
}